# ipfs publisher

publish to [ipfs](https://ipfs.io) key through remote host over ssh

## install

### through ipfs

`curl https://gateway.rabrux.space/ipfs/QmVe9SZAH7GM5ozf6kFfpoFPsXmE1x3FWZ9zTRELkdCUVk -LSso /usr/local/bin/ipfs-remote-publish`

### through gitlab

`curl https://gitlab.com/8topolar/ipfs-publisher/-/raw/master/publish -LSso /usr/local/bin/ipfs-remote-publish`

then make it executable

`chmod +x /usr/local/bin/ipfs-remote-publish`

> you should need to add `sudo` before each command`

## environment variables

| name | default | description |
| ---- | ------- | ----------- |
| `SSH_USER` | `root` | remote host username |
| `SSH_HOST` | `localhost` | remote host name or ip |
| `SSH_PORT` | `22` | remote host ssh port |

## usage

`ipfs-remote-publish <RESOURCE>`

where `<RESOURCE>` is the directory to be published.
